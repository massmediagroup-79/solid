<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Arr;

Route::get('game/start', 'Game\Web\GameController@index')->name('start.game');

Route::get('api/game/playing/{game}', 'Game\Web\GameController@showAPI');

Route::get('game/playing/{game}', 'Game\Web\GameController@show');
Route::post('game/', 'Game\Web\GameController@store')->name('store.game');
Route::put('game/{game}', 'Game\Web\GameController@update');

