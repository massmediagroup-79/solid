<?php


namespace App\Services;

use GuzzleHttp\Client;

/**
 * Class WinnerService
 * @package App\Services
 */
class guzzleGameService
{
    /**
     * @param $idGame
     * @return mixed
     */
    public function getData($idGame)
    {
        $http = new Client();
        $response = $http->get(config('app.api_path').'api/game/'.$idGame);
        return json_decode((string)$response->getBody(), true)[0];

    }


}
