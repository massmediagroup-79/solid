<?php
namespace App\Services;
use App\Models\Game;
use App\Services\GameService;
use Illuminate\Http\Request;

/**
 * Class WinnerService
 * @package App\Services
 */
class DatabaseGameService
{

    private $gameService;
    public function __construct( GameService $gameService)
    {
        $this->gameService = $gameService;
    }
    /**
     * @param $data
     * @return Game
     */
    public function store($data)
    {
        $item = new Game($data);
        $arr = [[null, null, null], [null, null, null], [null, null, null]];
        $item->fieldGame = json_encode($arr);
        $item->save();
        return $item;
    }

    /**
     * @param $data
     * @param $game
     * @return mixed
     */
    public function update($data, $game)
    {
        $arr = json_decode($game->fieldGame);
        $game->hit = $this->gameService->getHit($arr, 'X', '0');
        $arr[$data['x']][$data['y']] = $this->gameService->getHit($arr, 'X', '0');
        $game->result = $this->gameService->getWinner($arr, 'X', '0');
        $game->fieldGame = json_encode($arr);
        return $game->save();

    }

    /**
     * @param $data
     * @return bool
     */
    public function show($data)
    {
        $item = Game::findOrFail($data);
        if($item){
            $item=$item->toArray()[0];
            $item['fieldGame'] = json_decode($item['fieldGame']);
            return $item;
        }
        return false;

    }



}
