<?php

namespace App\Http\Controllers\Game\Api;

use App\Http\Requests\GameRequest;
use App\Models\Game;
use App\Services\DatabaseGameService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class GameController extends Controller
{
    private $databaseGameService;

    public function __construct(DatabaseGameService $databaseGameService)
    {
        $this->databaseGameService = $databaseGameService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return int
     */
    public function store(GameRequest $request)
    {
        $item = $this->databaseGameService->store($request->input());
        if ($item) {
            return response()->json($item->id, 201);
        }
        return 500;
    }

    /**
     * Display the specified resource.
     *
     * @param Game $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        return Game::findOrFail($game);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Game $game
     * @return int
     */
    public function update(Request $request, Game $game)
    {
        return response()->json(1, 200);
        $item = $this->databaseGameService->update($request->input(), $game);
        if ($item) {
            return response()->json($game, 200);
        }
        return 500;
    }
}
