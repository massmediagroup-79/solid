<?php

namespace App\Http\Controllers\Game\Web;

use App\Http\Requests\GameRequest;
use App\Models\Game;
use App\Http\Controllers\Controller;
use App\Services\DatabaseGameService;
use App\Services\GuzzleGameService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GameController extends Controller
{
    private $databaseGameService;

    public function __construct(DatabaseGameService $databaseGameService)
    {
        $this->databaseGameService = $databaseGameService;
    }

    public function index()
    {
        return view('game.index');
    }

    public function showAPI(Game $game, GuzzleGameService $guzzleGameService)
    {
        $item = $guzzleGameService->getData($game->id);
        $item['fieldGame'] = json_decode($item['fieldGame']);
        if ($item) {
            return view('game/game', compact('item'));
        }
    }

    public function show(Game $game)
    {

        $item = $this->databaseGameService->show($game);
        if ($item) {
            return view('game/game', compact('item'));
        }
    }


    public function store(GameRequest $request)
    {
        $item = $this->databaseGameService->store($request->input());
        if ($item) {
           return response()->json($item->id, 201);
        }
    }

    public function update(Request $request, Game $game)
    {
        $item = $this->databaseGameService->update($request->input(), $game);
        if ($item) {
            return response()->json($game, 200);
        }
    }


}


