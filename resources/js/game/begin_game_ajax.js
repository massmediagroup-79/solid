class BeginGameAjax {
    constructor(formId) {
        $(formId).submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function (idGame) {
                    $(location).attr('href', $("#storeGame").val() + "/game/playing/" + idGame);

                },
                error: function (response) {
                    if (response.status === 422) {
                        let errors = '<span id=\"errors\"><p class=\"text-danger\">';
                        $.each(response.responseJSON.errors, function (key, value) {
                            errors += value[0] + '<br>';
                        });
                        errors += '</p></span> ';
                        $('#errors').remove();
                        $('#msg-errors').append(errors);
                    }
                }
            });
        });
    }
}
export default BeginGameAjax;