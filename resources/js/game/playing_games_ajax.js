class PlayingGamesAjax {
    constructor(tableId) {

        $(() => {
            $(`${tableId} td`).on('click', function () {
                const data = {
                    'x' : $(this).parent('tr').index(),
                    'y' : $(this).parent().children().index($(this)),
                };
                $.ajax({
                    url: $("#updateGame").val() + '/game/' + window.location.pathname.split("/").pop(),
                    type: 'put',
                    data: data,
                    dataType: 'json',
                    context: this,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $(this).addClass(`player${data.hit}`);
                        $(this).off('click');
                        if(data.result)
                        {
                            $(`${tableId} td`).off('click');
                        }
                    },
                });
            });
        });
    }
}

export default PlayingGamesAjax;
