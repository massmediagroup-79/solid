@extends('layouts.app')
@section('content')
    <h3>Playing the game: {{$item['namePlayer1']}} and {{$item['namePlayer2']}}</h3>
    <h1 id="inf-player"></h1>
    <br/>
    <input type="hidden" id="updateGame" value="">
    <table border="2" id="click-table"  @if($item['result']) class="block-table" @endif>
        @for ($i = 0; $i < 3; $i++)
            @component('components.tableComponent', ['status' => $item['fieldGame'], 'iterator' => $i])
            @endcomponent
        @endfor
    </table>
    <br>
    <a class="navbar-brand" href="{{route('start.game')}}">Start the game again</a>
@endsection
