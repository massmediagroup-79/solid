@extends('layouts.app')
@section('content')
    @include('includes.result_messages')
    <form id="beginGame" method="post" action="{{route(config('app.storeRoute'))}}">
        @csrf
        <input type="hidden" id="storeGame" value="">
        @component('components.inputComponent',['idElement' => 'player1','description' =>'Player 1:','namePlayer' =>'namePlayer1'])
        @endcomponent
        @component('components.inputComponent',['idElement' => 'player2','description' =>'Player 2:','namePlayer' =>'namePlayer2'])
        @endcomponent
        <button type="submit" class="btn btn-primary">Start</button>
        <div id='msg-errors'></div>
    </form>
@endsection
